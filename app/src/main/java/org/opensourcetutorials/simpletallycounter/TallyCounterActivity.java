package org.opensourcetutorials.simpletallycounter;

import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class TallyCounterActivity extends ActionBarActivity {

    public static final String STATE_CURRENT_TALLY = "current_tally";
    public static final String STATE_SOUND_ENABLE = "sound_enabled";
    public static final String STATE_VOL_KEY_USED = "volume_key_used";
    private int mCurrentTally = 0;

    private Button mBtnCount;

    private SoundPool mSoundPool;
    private int mSoundID;
    private static final float SOUND_ON = 1;
    private static final float SOUND_OFF = 0.0f;
    private static boolean SOUND_ENABLED = true;
    private static float SOUND_VOL;

    private static boolean USE_VOL_KEYS = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tally_counter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            mCurrentTally = savedInstanceState.getInt(STATE_CURRENT_TALLY);
            SOUND_ENABLED = savedInstanceState.getBoolean(STATE_SOUND_ENABLE);
            USE_VOL_KEYS = savedInstanceState.getBoolean(STATE_VOL_KEY_USED);
        }

        this.sound_enabled(SOUND_ENABLED);

        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        mSoundID = mSoundPool.load(this, R.raw.click, 1);

        mBtnCount = (Button) findViewById(R.id.button_count);
        mBtnCount.setText(getTallyCountDisplay(mCurrentTally));

        mBtnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countUp();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tally_counter, menu);

        MenuItem sound_option = menu.findItem(R.id.action_toggle_sound);
        sound_option.setChecked(SOUND_ENABLED);

        MenuItem vol_key_option = menu.findItem(R.id.action_use_volume_keys);
        vol_key_option.setChecked(USE_VOL_KEYS);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_reset:
                mCurrentTally = 0;
                mBtnCount.setText(getTallyCountDisplay(mCurrentTally));
                mSoundPool.play(mSoundID, SOUND_VOL, SOUND_VOL, 1, 0, 0.75f);
                return true;
            case R.id.action_subtract:
                countDown();
                return true;
            case R.id.action_toggle_sound:
                if (item.isChecked()) {
                    item.setChecked(false);
                    sound_enabled(false);
                } else {
                    item.setChecked(true);
                    sound_enabled(true);
                }
                return true;
            case R.id.action_use_volume_keys:
                if (item.isChecked()) {
                    item.setChecked(false);
                    USE_VOL_KEYS = false;
                } else {
                    item.setChecked(true);
                    USE_VOL_KEYS = true;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (USE_VOL_KEYS) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    countDown();
                    return true;
                case KeyEvent.KEYCODE_VOLUME_UP:
                    countUp();
                    return true;
                default:
                    return super.onKeyDown(keyCode, keyEvent);
            }
        }
        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putInt(STATE_CURRENT_TALLY, mCurrentTally);
        savedInstanceState.putBoolean(STATE_SOUND_ENABLE, SOUND_ENABLED);
        savedInstanceState.putBoolean(STATE_VOL_KEY_USED, USE_VOL_KEYS);

        super.onSaveInstanceState(savedInstanceState);
    }

    private String getTallyCountDisplay(int tally) {
        if (tally == 10000 || tally < 0) {
            tally = 0;
            mCurrentTally = 0;
        }

        return String.format("%04d", tally);
    }

    private void sound_enabled(boolean enable) {
        if (enable) {
            SOUND_VOL = SOUND_ON;
        } else {
            SOUND_VOL = SOUND_OFF;
        }
        SOUND_ENABLED = enable;
    }

    private void countDown() {
        mBtnCount.setText(getTallyCountDisplay(--mCurrentTally));
        mSoundPool.play(mSoundID, SOUND_VOL, SOUND_VOL, 1, 0, 0.5f);
    }

    private void countUp() {
        mBtnCount.setText(getTallyCountDisplay(++mCurrentTally));
        mSoundPool.play(mSoundID, SOUND_VOL, SOUND_VOL, 1, 0, 1);
    }
}
